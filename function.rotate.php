<?php
	
	/*

	function.rotate.php
	--------------------

	@file 		function.rotate.php
	@version 	0.3
	@date 		11-29-2010
	@author 	Kenton Glass [hi@kenton.me]

	Copyright (c) 2010 Kenton Glass [http://kenton.me]

	*/
	
	# Rotate Function
	function smarty_cms_function_rotate($params, &$smarty){
	
		# Default Plugin Values
		$default['path']   = 'uploads/images/'; # uploads/images/
		$default['id']	   = 'rotate'; # rotating div id (must be defined for multiple slideshows
		$default['width']  = ''; # 800px
		$default['height'] = ''; # 200px
		$default['max']    = 10; # 10
		$default['debug']  = 0; # 0, false
		
		# Default Cycle Options
		$default['effect'] = 'fade'; # See http://jquery.malsup.com/cycle/browser.html for all effects.
		$default['speed']  = '2000'; # 2000
		$default['delay']  = '5000'; # 5000
		
		# Div ID
		if(!empty($params['id'])):
			$id = $params['id'];
		else:
			$id = $default['id'];
		endif;
	
		# Width
		if(!empty($params['width'])):
			$width = 'width="'.$params['width'].'px"';
		else:
			if(!empty($default['width'])):
				$width = ' width="'.$default['width'].'"';
			endif;
		endif;
	
		# Height
		if(!empty($params['height'])):
			$height = 'height="'.$params['height'].'px"';
		else:
			if(!empty($default['height'])):
				$height = ' height="'.$default['height'].'"';
			endif;
		endif;
		
		# Div Height
		if(!empty($params['height'])):
			$div_height = 'height:'.$params['height'].'px;';
		else:
			if(!empty($default['height'])):
				$div_height = ' height:'.$default['height'].';';
			endif;
		endif;
		
		# Max Images
		if(!empty($params['max'])):
			$max = $params['max'];
		else:
			$max = $default['max'];
		endif;
 
		# Path
		if(!empty($params['path'])):
			$path = $params['path'];
		else:
			$path = $default['path'];
		endif;
		
		# Allowed File Types
		$file_types = array('.jpg', '.gif', '.png');
	
		# Start Array
 		$image_list = array();
 
		# Check Path
		if(is_dir($path)):
 		
 			# Try to Open Directory
	 		$handle = opendir($path);
		
			# Read All Files in Directory
 			while(false !== ($file = readdir($handle))):
  			
  				# Not Hidden Files or Thumbnails
  				if($file != '.' && $file != '..' && !preg_match('/^thumb_/', $file)):
  				
  					# Get Exentension
   					$extension = strtolower(substr($file,-4));
   				
   					# Check Image
   					if(in_array($extension, $file_types)):
    				
   		 				# Add To Array
    					$image_list[] = $file;
  				
  					# End Check
  					endif;
 		
 				# End Not Hidden or Thumnails
 				endif;
 		
 			# End Loop
 			endwhile;
 		
 			# Close Directory
 			closedir($handle);
	
		# End Check Path
		endif;
 
		# Count Under Max
		if(count($image_list) < $max):
		
			# Set To Actual Count
			$max = count($image_list);
	 	
		# End Count Under Max
		endif;
		
		# Effect
		if(!empty($params['effect'])):
			$effect = $params['effect'];
		else:
			$effect = $default['effect'];
		endif;
		
		# Delay
		if(!empty($params['delay'])):
			$delay = $params['delay'];
		else:
			$delay = $default['delay'];
		endif;
		
		# Speed
		if(!empty($params['speed'])):
			$speed = $params['speed'];
		else:
			$speed = $default['speed'];
		endif;
		
		# Start Div
		print '<div id="'.$id.'" style="display:none;'.$div_height.'">'."\n";
		
		# Random Max Images
		$random = array_rand($image_list, $max);
 
		# Loop Images
		foreach($random as $image):
 		
 			# Print Image"
 			print '		<img src="'.$path.$image_list[$image].'" '.$width.' '.$height.' alt="'.$image_list[$image].'" />'."\n";
 	
 		# End Loop
 		endforeach;
 		
 		# End Div
 		print '</div>'."\n";
 		
 		# jQuery
		print '<script type="text/javascript">$(document).ready(function(){$(\'#'.$id.'\').css(\'display\',\'block\');$(\'#'.$id.'\').cycle({fx:\''.$effect.'\',timeout:'.$delay.',speed:'.$speed.'});});</script>';
		
		# Debug
		if($params['debug']):
			
			# Print HTML Comment of Cycle Options
			print "\n".'<!--'."\n"
				 .'	Path: '.$path."\n"
				 .' Effect: '.$effect."\n"
				 .'	Delay: '.$delay."\n"
				 .'	Speed: '.$speed."\n"
				 .'	Max: '.$max."\n"
				 .'-->'."\n";
		
		# End Debug	
		endif;
	
	# End Rotate Function
	}
	
	# Help Function
	function smarty_cms_help_function_rotate(){

?>
<h3>What is Rotate?</h3>
	<p>Rotate is a plugin that creates a slideshow from a directory of images using the <a href="http://jquery.malsup.com/cycle/" rel="external">jQuery Cycle Plugin</a>.</p>
<h3>How do I use it?</h3>
	<p>Make sure to include the jQuery script, as well as the Cycle Script. Then just place the following tag in your template or content: <code>{rotate}</code></p>
<h3>What parameters can I use with Rotate?</h3>
	<p>
		<ul>
			<li><em>(optional)</em> <tt>path</tt> - Relative URL to pick images from (default is uploads/images/).</li>
			<li><em>(optional)</em> <tt>id</tt> - Unique ID for rotating div (default is rotate... must be unique for multiple uses on one page).</li>
			<li><em>(optional)</em> <tt>effect</tt> - Transition Effect (default is fade; see <a href="http://malsup.com/jquery/cycle/browser.html" rel="external">Effects Browser</a> for available effects).</li>
			<li><em>(optional)</em> <tt>delay</tt> - Interval between images (default is 5 seconds).</li>
			<li><em>(optional)</em> <tt>speed</tt> - Transition speed (default is 2 seconds).</li>
			<li><em>(optional)</em> <tt>height</tt> - Image height (default is unspecified, eg. the actual image height).</li>
			<li><em>(optional)</em> <tt>width</tt> - Image width (default is unspecified, eg. actual image width).</li>
			<li><em>(optional)</em> <tt>max</tt> - Max number of images to display (default is 10).</li>
			<li><em>(optional)</em> <tt>debug</tt> - Prints out parameters in an HTML comment (set 1 or true for use).</li>
		</ul>
	</p>
	<p>You can change every parameter default by editing line 20-31 of <em>function.rotate.php</em>.</p>
<h3>Javascript</h3>
	<p>Make sure to include the following scripts before using {rotate}. You need to upload cycle.js to your server and change /PATH/TO/ to it's correct path.</p>
	<ul>
		<li><code>&lt;script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"&gt;&lt;/script&gt;</code></li>
		<li><code>&lt;script type="text/javascript" src="/PATH/TO/cycle.js"&gt;&lt;/script&gt;</code></li>
	</ul>
<?php 

	# End Help Function
	}
	
	# About Function
	function smarty_cms_about_function_rotate(){

?>
<p>Author: <a href="http://kenton.me/" rel="external">Kenton Glass</a></p>
<p>Cycle Plugin from <a href="http://jquery.malsup.com/cycle/" rel="external">malsup</a> and licensed under the <a href="http://www.opensource.org/licenses/mit-license.php" rel="external">MIT License</a>.</p>
<p>Version: 0.3</p>
<br />
<p>Changes:</p>
<p>0.3 - Added unique ID for rotating Div. Removed .rotate class. Updated version of included cycle.js.</p>
<p>0.2.2 - Removed hardcoded px to prevent problems with IE6. Added exclusion for CMSMS thumbnails. Added debug parameter to print out set parameters.</p>
<p>0.2.1 - Created Full &amp; Lite Versions. Changed jQuery load from css.block to fadein.</p>
<p>0.2 - Added full cycle effects. Upload included cycle.js to use different effects. You can use the 'effect' parameter to specify the effect.</p>
<p>0.1.1 - Fixed some code formatting.</p>
<p>0.1 - Initial release.</p>
<?php

	# End About Function
	}
	
?>