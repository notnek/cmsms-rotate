Rotate
======

Rotate is a CMS Made Simple plugin that creates a slideshow from a directory of images using the jQuery Cycle plugin.

How do I use it?
----------------

Make sure to include the jQuery script, as well as the Cycle Script. Then just place the following tag in your template or content:

```html
{rotate}
```

What parameters can I use with Rotate?
--------------------------------------

+ (optional) path   - Relative URL to pick images from (default is "uploads/images/")
+ (optional) effect - Transition effect (default is fade)
+ (optional) delay  - Interval between images (default is 5 seconds)
+ (optional) speed  - Transition speed (default is 2 seconds)
+ (optional) height - Image height (default is unspecified)
+ (optional) width  - Image width (default is unspecified)
+ (optional) max    - Max number of images to display (default is 10)
+ (optional) debug  - Prints out parameters in an HTML comment (set 1 or true for use)

You can change the default parameters by editing line 21-29 of function.rotate.php.

Javascript
----------

Make sure to include the following scripts before using {rotate}. You need to upload cycle.js to your server and change /PATH/TO/ to it's correct path.

```html
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/PATH/TO/cycle.js"></script>
```